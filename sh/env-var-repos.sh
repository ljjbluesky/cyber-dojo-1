# Some of these have a dependency order

CYBER_DOJO_REPOS=(rack-base \
                  docker-base \
                  web-base \
                  prometheus \
                  grafana \
                  starter-base \
                  custom \
                  exercises \
                  languages \
                  zipper \
                  differ \
                  saver \
                  mapper \
                  runner \
                  ragger \
                  web \
                  nginx \
                  versioner \
                  commander)
